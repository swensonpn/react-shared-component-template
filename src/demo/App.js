import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button } from '../lib';

export class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/demo/App.js</code> and save to reload.
        </p>
        <p>
          Write example usages of your custom components here
        </p>
        <Button/>
      </div>
    );
  }
}
