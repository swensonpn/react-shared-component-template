module.exports = {
  entry: { main: "./src/lib" },
  output: {
    path: __dirname + "/dist",
    filename: "<your dist filename here>",
    libraryTarget: "umd",
    library: "<your module name here>",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: { loader: "babel-loader" },
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      }
    ],
  },
  externals: {
    react: "react"
  },
};
